# FER Timetable Validator

This program will check for the timetable output.

```
The program can be started using this parameters:
 Available program parameters:
   -debug 0     => Hide error message and detailed errors (default: 1)
   -calculate 0 => Don't evaluate the solution - display total score (default: 1)
   -detailed 1  => Display detailed scoring evaluation (default: 0)
   -groupsize 1 => Display number of students in groups after the swap is performed (default: 0)
   -verify 0    => Dont verify the solution (default: 1)
   -maxvalue 0  => Display maximal available score (default: 1)
 
 Parameters for algorithm:
   -award-student  0,2,4    => Additional score for swap combinations for a single student (default: 1,2,3,4)
   -minmax-penalty 2        => Penalty for over or below max_preffered and min_preffered (default: 1)
   -students-file stud.csv  => Name of students file (default: students.csv)
   -requests-file reqs.csv  => Name of requests file (default: requests.csv)
   -overlaps-file over.csv  => Name of overlaps file (default: overlaps.csv)
   -limits-file limt.csv    => Name of limits file (default: limits.csv)
 ```

 # Docker

How to use the docker image to build the source code and to run in afterwards:

Building the docker image:
```
 cd <locationofdockerfile>
 docker build -t monoimage .
```

Starting the image with local data - local data is stored in samples folder:
```
cd <locationofdockerfile>
docker run -v $PWD/samples/:/samples/  -t monoimage "mono" "/FER-Timetable-Validator/FER-Timetable-Validator/bin/Debug/FER-Timetable-Validator.exe" -detailed 1 -groupsize 1 -students-file /samples/student.csv
```

Previous example assumes that you have folder "samples/" with all the required files inside. 
By default it contains a simple sample for testing the result of the software. If your samples 
are stored in another folder you can map the full path name by changing -v parameter.


