FROM mono:latest
ADD FER-Timetable-Validator .
ADD samples /samples
RUN nuget restore FER-Timetable-Validator.sln
RUN msbuild FER-Timetable-Validator.sln
WORKDIR /samples
CMD [ "mono", "/FER-Timetable-Validator/FER-Timetable-Validator/bin/Debug/FER-Timetable-Validator.exe" ]
